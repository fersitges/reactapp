<?php
$mysqli = new mysqli(
    'localhost',
    'root',
    'mexico',
    'articles',
    3307
);

$filter = (int) $_GET['filter'];
$filterquery = '';

if ($filter != 0) {
    $filterquery = 'WHERE category=' . $filter;
}

$query = <<<SQL
    SELECT * FROM articles {$filterquery}
SQL;

$result = $mysqli->query($query);
$totalrows = $result->num_rows;
$articles = [];
while ($row = $result->fetch_assoc()) {
    $articles[] = $row;
}

echo json_encode($articles);