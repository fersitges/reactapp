import React from 'react';
import ReactDOM from 'react-dom';
import { Header, Icon, Tab, Card, Label, Button, Segment,
         Image, Container, Grid, Select, Modal } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import './index.css';

/**
 * Main app component
 */
class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            filter: 0,
            articles: [],
            showdetails: false,
            articleid: false
        };
    }

    render() {
        let panes = [], items = ['All', 'God', 'Goddess', 'Mortal'];

        for (let i = 0; i < 4; i++) {
            panes.push({
                menuItem: items[i],
                render: () => this._tabPane(i)
            });
        }

        return(
            <Container fluid className="maincontent">
                <Header as='h2' textAlign='center' icon>
                    <Icon name='university' />
                    Mythology
                    <Header.Subheader>
                        A React Js Sample App
                    </Header.Subheader>
                </Header>
                <Tab
                    menu={{ color: 'blue', inverted: true, attached: false, tabular: false, pointing: true }}
                    panes={panes} onTabChange={this._filterList.bind(this)} />
            </Container>
        );
    }

    _tabPane(filter) {
        if (filter === this.state.filter) {
            return (
                <Tab.Pane>
                    <Header as='h3'>{this._getArticleCount(this.state.articlescount)}</Header>
                    <Grid stackable columns={3}>
                        <ArticleList
                            articles={this.state.articles}
                            articlescount={this.state.articlescount}
                        />
                    </Grid>
                </Tab.Pane>
            );
        } else return false;
    }

    _filterList(event, data) {
        event.preventDefault();
        let category = data.activeIndex;
//        fetch('services/services.php?filter=' + category)
        fetch('articles.json')
            .then( (response) => {
                return response.json() })
            .then( (json) => {
                this.setState({articles: json, articlescount: json.length, filter: category });
            });
    }

    _getArticleCount(articlecount) {
        if (articlecount === 0) {
            return 'There are no articles';
        } else if (articlecount === 1) {
            return '1 article';
        } else {
            return `${articlecount} articles`;
        }
    }

    componentDidMount() {
//        fetch('services/services.php', { filter: this.props.articleFilter })
        fetch('articles.json')
            .then( (response) => {
                return response.json() })
            .then( (json) => {
                this.setState({articles: json, articlescount: json.length });
            });
    }
}

/**
 * Article list component, contains each article card components
 */
class ArticleList extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <Grid.Row>
                {this._getArticles()}
            </Grid.Row>
        );
    }

    _getArticles() {
        return this.props.articles.map((article) => {
            return (
                <Article
                    key={article.id}
                    id={article.id}
                    name={article.name}
                    description={article.description}
                    rating={article.rating}
                    category={article.category}
                    pic={article.pic}
                    onSelectArticle={this._handleChange}
                />
            );
        });
    }

    _handleChange(value) {
        console.log(value, this);
    }
}

class Article extends React.Component {
    constructor(props) {
        super(props);
        this.handleHover = this.handleHover.bind(this);
        this.handleOut = this.handleOut.bind(this);
        this.state = {
            hovered: false,
            rating: false,
            opendetails: false
        }
    }

    render() {
        const rates = [
            { key: '1', value: '1', text: '1 star' },
            { key: '2', value: '2', text: '2 stars' },
            { key: '3', value: '3', text: '3 stars' },
            { key: '4', value: '4', text: '4 stars' },
            { key: '5', value: '5', text: '5 stars' }
        ];

        return(
            <Grid.Column>
                <Card className="article-card"
                      onMouseEnter={this.handleHover}
                      onMouseLeave={this.handleOut}
                      onClick={this.handleSelectDetails.bind(this)}>
                    <Image className="img img-responsive" src={this.props.pic}
                           alt={this.props.name} />
                    <Card.Content>
                        <Card.Header>
                            {this.props.name}
                        </Card.Header>
                        <Card.Description>
                            {this.props.description}
                        </Card.Description>
                    </Card.Content>
                    <Card.Content extra>
                        {this.state.rating === false &&
                            <Label basic onClick={this.handleRate.bind(this)}>
                                {this._getArticleStars(this.props.rating)}
                            </Label>
                        }
                        {this.state.rating === false &&
                            <Label color={this.state.hovered ? 'blue' : 'grey'}
                                   onClick={this.handleRate.bind(this)}>
                            Rate me!
                            </Label>
                        }
                        { this.state.rating === true &&
                            <Select placeholder='How many stars?'
                                    options={rates}
                                    onChange={this.handleSelectRate.bind(this)}
                            />
                        }
                    </Card.Content>
                </Card>
            </Grid.Column>
        )
    }

    _getArticleStars(stars) {
        let starsicons = [];
        for (let i=0; i<stars;i++) {
            starsicons.push(<Icon name="star" key={i} />);
        }
        return starsicons;
    }

    handleHover() {
        this.setState({hovered: true});
    }

    handleOut() {
        this.setState({hovered: false});
    }

    handleRate(event) {
        event.preventDefault();
        this.setState({rating: true});
    }

    handleSelectRate() {
        this.setState({rating: false});
    }

    handleSelectDetails() {
        this.props.onSelectArticle(this.props.id);
    }
}

class ArticleDetailsModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalOpen: props.showdetails,
            articleid: props.articleid
        }
    }

    handleClose = (e) => this.setState({
        modalOpen: false,
    });

    render() {
        return(
            <Modal open={this.state.modalOpen}
                   onClose={this.handleClose}
                   closeIcon='close'>
                <Header icon='archive' content='Archive Old Messages' />
                <Modal.Content>
                    <p>{this.props.articleid}</p>
                </Modal.Content>
                <Modal.Actions>
                    <Button color='red'>
                        <Icon name='remove' /> No
                    </Button>
                    <Button color='green'>
                        <Icon name='checkmark' /> Yes
                    </Button>
                </Modal.Actions>
            </Modal>
        );
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('root')
);