const articlesService = [
    {
        id: 11,
        name: 'Minerva',
        description: 'The c-beam is wisely human. Space suits reproduce on vision at nowhere! Crazy mankinds lead to the anomaly. Walk pedantically like a seismic sun. View cunningly like a cold kahless. Bare honors lead to the understanding. The parasite is more crewmate now than emitter. photonic and nosily fantastic?',
        rating: 4,
        category: 2,
        pic: 'https://unsplash.it/400/200?image=1011'
    },
    {
        id: 12,
        name: 'Zeus',
        description: 'Creatures wobble from courages like devastated hurqs. Tremble revolutionary like a ugly klingon.I accelerate this turbulence, it\'s called bare life. All those adventures will be lost in lifes like histories in cores.',
        rating: 5,
        category: 1,
        pic: 'https://unsplash.it/400/200?image=1012'
    },
    {
        id: 13,
        name: 'Venus',
        description: 'The tribble walks sonic shower like a carnivorous star. All the starships evacuate virtual, sub-light teleporters. Moon at the planet was the peace of assimilation, invaded to a conscious particle!',
        rating: 2,
        category: 2,
        pic: 'https://unsplash.it/400/200?image=1013'
    },
    {
        id: 14,
        name: 'Prometeo',
        description: 'Countless starlight travels will be lost in beauties like plasmas in collision courses. Harvest wihtout death, and we won’t assimilate a collective. Harmless, sub-light crews tightly accelerate an extraterrestrial, ugly planet.',
        rating: 3,
        category: 3,
        pic: 'https://unsplash.it/400/200?image=1014'
    },
    {
        id: 15,
        name: 'Jupiter',
        description: 'Admirals go with peace at the colorful holodeck! When the processor experiments for subspace, all teleporters manifest ship-wide, cold space suits. Crews wobble with collision course.',
        rating: 5,
        category: 1,
        pic: 'https://unsplash.it/400/200?image=1015'
    },
    {
        id: 16,
        name: 'Marte',
        description: 'X-ray vision at the space station that is when photonic astronauts experiment. Walk wihtout disconnection, and we won’t offer a tribble. Vision at the solar system that is when carnivorous captains tremble!',
        rating: 2,
        category: 1,
        pic: 'https://unsplash.it/400/200?image=1016'
    },
    {
        id: 17,
        name: 'Eneas',
        description: 'Energy at the space station was the tragedy of honor, infiltrated to a virtual pathway. Love at the galaxy was the definition of core, consumed to a neutral emitter.',
        rating: 1,
        category: 3,
        pic: 'https://unsplash.it/400/200?image=1017'
    },
    {
        id: 18,
        name: 'Odiseo',
        description: 'C-beams tremble on metamorphosis at captain\'s quarters! Adventure at the parallel universe was the ellipse of flight, converted to a spheroid ferengi!',
        rating: 4,
        category: 3,
        pic: 'https://unsplash.it/400/200?image=1018'
    },
    {
        id: 19,
        name: 'Jano',
        description: 'Friendship at the solar sphere was the sensor of honor, controlled to a vital creature. Sonic showers harvest with flight! It is a chemical turbulence, sir. The captain is more planet now than creature. cloudy and pedantically dead.',
        rating: 1,
        category: 1,
        pic: 'https://unsplash.it/400/200?image=1019'
    },
    {
        id: 20,
        name: 'Ceres',
        description: 'Disconnection, procedure, and friendship. Go wihtout friendship, and we won’t accelerate a queen. Adventure at the alpha quadrant was the galaxy of wind, attacked to a united space suit.',
        rating: 3,
        category: 2,
        pic: 'https://unsplash.it/400/200?image=1020'
    },
    {
        id: 21,
        name: 'Heracles',
        description: 'Fly finally like a bare species. This pattern has only been teleported by a greatly exaggerated dosi. All those stigmas will be lost in mysteries like hypnosis in powerdrains.',
        rating: 4,
        category: 3,
        pic: 'https://unsplash.it/400/200?image=1021'
    },
    {
        id: 22,
        name: 'Perseo',
        description: 'Wisely, indeed, bare rumour! Transformators die with beauty at the boldly colony! Plasma at the parallel universe was the definition of coordinates, lowered to a photonic alien.',
        rating: 3,
        category: 3,
        pic: 'https://unsplash.it/400/200?image=1022'
    },
    {
        id: 23,
        name: 'Medusa',
        description: 'It is an extraterrestrial procedure, sir. When the dosi malfunctions for starfleet headquarters, all starships deceive sub-light, futile green people. Warp wihtout turbulence, and we won’t yearn a hurq.',
        rating: 1,
        category: 3,
        pic: 'https://unsplash.it/400/200?image=1023'
    },
    {
        id: 24,
        name: 'Dédalo',
        description: 'Walk wihtout paralysis, and we won’t desire an emitter. Neutral patterns lead to the honor. The space suit is more girl now than phenomenan. spheroid and cunningly clear. None of these space suits command neutral, twisted admirals.',
        rating: 3,
        category: 3,
        pic: 'https://unsplash.it/400/200?image=1024'
    },
    {
        id: 25,
        name: 'Hefesto',
        description: 'The machine is more alien now than particle. modern and virtually biological. Strange particles, to the moon. Lieutenant commanders are the sensors of the bare courage. This collision course has only been teleported by a futile pathway.',
        rating: 5,
        category: 1,
        pic: 'https://unsplash.it/400/200?image=1025'
    },
    {
        id: 26,
        name: 'Apolo',
        description: 'Coordinates at the solar sphere was the procedure of courage, transformed to a dead starship.Procedure at the solar sphere was the x-ray vision of shield, fighted to a human proton. The chemical mermaid pedantically outweighs the cosmonaut.',
        rating: 2,
        category: 1,
        pic: 'https://unsplash.it/400/200?image=1026'
    },
    {
        id: 27,
        name: 'Hera',
        description: 'Hur\'qs are the space suits of the ordinary mineral. When the planet meets for atlantis tower, all klingons contact senior, strange emitters. Reliable, gravimetric ships finally acquire a united, devastated species.',
        rating: 4,
        category: 2,
        pic: 'https://unsplash.it/400/200?image=1027'
    }
];

export default articlesService;